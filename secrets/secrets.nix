let
  laptop_helios = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFQWfVKls31yK1aZeAu5mCE+xycI9Kt3Xoj+gfvEonDg NixOS Laptop";
  laptop_aether = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOmm4CCnpT+tF7vecSrku0+7aDA1z3pQ+PDqZvoCynCR silver@aether";
  desktop = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN34yTh0nk7HAz8id5Z/wiIX3H7ptleDyXy5bfbemico Desktop";
  desktop_wsl = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBHbsOjxIcLasz+CHA8gUg1pvc8dPHwMKdWoIwNvPxLp Desktop_WSL";

  users = [
    laptop_helios
    laptop_aether
    desktop
    desktop_wsl
  ];

  terra = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPHTFVt1V4t+donSwSbPtnERqgTAtmxWzD9EIsjWcn8D root@terra";

  systems = [
    terra
  ];

  gitlab = [
    terra
  ];
in {
  # nix run github:ryantm/agenix -- -e secret1.age
  "gitlab/pw.age".publicKeys = users ++ gitlab;
  "gitlab/db_pw.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_db.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_secret.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_otp.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_jws.age".publicKeys = users ++ gitlab;
  "gitlab/app_id.age".publicKeys = users ++ gitlab;
  "gitlab/app_secret.age".publicKeys = users ++ gitlab;

  "dns/cloudflare.age".publicKeys = users ++ systems;

  "monica/appKeyFile.age".publicKeys = users ++ systems;
  "monica/passwordFile.age".publicKeys = users ++ systems;

  "wireguard/privateKeyFile.age".publicKeys = users ++ systems;

  "vaultwarden/environmentFile.age".publicKeys = users ++ systems;
}
