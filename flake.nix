{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.11";

    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.91.1-1.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # utility stuff
    home-manager.url = "github:nix-community/home-manager/master";
    flake-utils.url = "github:numtide/flake-utils";
    agenix.url = "github:ryantm/agenix";
    colmena.url = "github:zhaofengli/colmena";
    nixos-wsl.url = "github:nix-community/NixOS-WSL/main";

    openfortivpn-cli.url = "github:emilioziniades/openfortivpn-cli";
    openfortivpn-cli.inputs.nixpkgs.follows = "nixpkgs";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    # for fingerprint reader
    #    nixos-06cb-009a-fingerprint-sensor = {
    #      url = "github:ahbnr/nixos-06cb-009a-fingerprint-sensor";
    #      #inputs.nixpkgs.follows = "nixpkgs";
    #    };

    alejandra = {
      url = "github:kamadorueda/alejandra";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    programsdb = {
      url = "github:wamserma/flake-programs-sqlite";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    alejandra,
    agenix,
    colmena,
    ...
  } @ inputs: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
  in {
    formatter.x86_64-linux = alejandra.defaultPackage."x86_64-linux";

    devShells.x86_64-linux.default = pkgs.mkShell {
      name = "Silver build env";
      nativeBuildInputs = [
        pkgs.buildPackages.git
        colmena.defaultPackage."x86_64-linux"
        pkgs.buildPackages.nmap
      ];
      buildInputs = [agenix.packages.x86_64-linux.default];
      shellHook = ''export EDITOR="${pkgs.nano}/bin/nano --nonewlines"; unset LD_LIBRARY_PATH;'';
    };

    colmena = {
      meta = {
        nixpkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = [];
        };
        specialArgs = {
          inherit inputs;
        };
      };

      # installed for each machine
      defaults = import ./machines/base.nix;

      # Xarbon X1 (6th gen) thinkpad
      # colmena apply-local --sudo
      helios = import ./machines/helios/configuration.nix;

      # homelab (lxc)
      # colmena apply --on terra
      terra = import ./machines/terra/configuration.nix;

      # WSL in my main desktop
      # colmena apply-local --sudo
      ananke = import ./machines/ananke/configuration.nix;

      # Frame
      aether = import ./machines/aether/configuration.nix;
    };
  };
}
