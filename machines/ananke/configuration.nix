# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).
# NixOS-WSL specific options are documented on the NixOS-WSL repository:
# https://github.com/nix-community/NixOS-WSL
{
  lib,
  inputs,
  ...
}: let
  name = "ananke";
  ip_pub = "172.26.112.1";
in {
  imports = [
    inputs.nixos-wsl.nixosModules.default
  ];

  wsl.enable = true;
  wsl.defaultUser = "silver";

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = "root";
    allowLocalDeployment = true;
    tags = [];
  };

  networking.hostName = name;

  # will need remote access
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "prohibit-password";
  };

  services.silver.mount_media = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = lib.mkForce "24.05"; # Did you read the comment?
}
