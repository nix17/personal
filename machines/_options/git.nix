{...}: {
  home-manager.users.silver.programs.git = {
    enable = true;
    userName = "Brendan Golden";
    userEmail = "git@brendan.ie";
    extraConfig = {
      init.defaultBranch = "main";
      pull.rebase = true;
    };
    lfs.enable = true;
  };
}
