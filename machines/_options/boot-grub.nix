{...}: {
  boot.loader.grub.enable = true;
  boot.loader.grub.useOSProber = true;
}
