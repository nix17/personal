{...}: {
  home-manager.users.silver.programs.git.signing = {
    key = "5DE30D67F4D7D4ED8ED976EDB641DD64F4B15C58";
    signByDefault = true;
  };
}
