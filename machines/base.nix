{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: let
  cfg = config.services.silver;
in {
  imports = [
    # for the secrets
    inputs.agenix.nixosModules.default
    inputs.lix-module.nixosModules.default
  ];

  options.services.silver = {
    mount_media = lib.mkOption {
      default = true;
      type = lib.types.bool;
      description = ''
        Mount /mnt/Media
      '';
    };
  };

  config = {
    # may as well be on the latest kernel
    #boot.kernelPackages = pkgs.linuxPackages_latest;

    # for command-not-found to work with flakes
    programs.command-not-found.dbPath = inputs.programsdb.packages.${pkgs.system}.programs-sqlite;

    nixpkgs.config.permittedInsecurePackages = [
      "dotnet-sdk-6.0.428"
      "aspnetcore-runtime-6.0.36"
    ];

    nix = {
      settings = {
        # flakes are essensial
        experimental-features = ["nix-command" "flakes"];
        trusted-users = [
          "root"
          "silver"
        ];
      };

      # to free up to 10GiB whenever there is less than 1GiB left
      extraOptions = ''
        min-free = ${toString (1024 * 1024 * 1024)}
        max-free = ${toString (1024 * 1024 * 1024 * 10)}
      '';
    };

    # media server when connected to home
    fileSystems."/mnt/Media" = lib.mkIf cfg.mount_media {
      device = "192.168.1.5:/mnt/Media/";
      fsType = "nfs";
      options = [
        "rw"
        "hard"
        # "user" "x-systemd.automount" "noauto"
      ];
    };

    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users = {
      root = {
        initialHashedPassword = "";

        openssh.authorizedKeys.keys = [
          # no obligation to have name attached to keys

          # Root account
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL+IT+J7n6IF0m/Jh9ZkifmhocOjavd4qfMnpE+apE2u root@terra"
        ];
      };

      silver = {
        isNormalUser = true;
        description = "Brendan Golden";
        extraGroups = ["networkmanager" "wheel"];
      };
    };

    security.sudo.extraRules = [
      {
        groups = ["wheel"];
        commands = [
          {
            command = "ALL";
            options = ["NOPASSWD"];
          }
        ];
      }
    ];

    # Allow unfree packages
    nixpkgs.config.allowUnfree = true;

    # List packages installed in system profile. To search, run:
    environment.systemPackages = with pkgs; [
      git
      git-lfs
      bind
      traceroute
      htop
      ncdu_2
      nano
    ];

    # Some programs need SUID wrappers, can be configured further or are
    # started in user sessions.
    # programs.mtr.enable = true;
    # programs.gnupg.agent = {
    #   enable = true;
    #   enableSSHSupport = true;
    # };

    # List services that you want to enable:

    # Enable the OpenSSH daemon.
    # services.openssh.enable = true;

    # Open ports in the firewall.
    # networking.firewall.allowedTCPPorts = [ ... ];
    # networking.firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # networking.firewall.enable = false;

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "23.05"; # Did you read the comment?
  };
}
