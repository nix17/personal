{
  inputs,
  lib,
  pkgs,
  ...
}: let
  name = "aether";
  ip_pub = "192.168.1.143";
in {
  imports = [
    inputs.nixos-hardware.nixosModules.framework-13-7040-amd
    ./hardware-configuration.nix
    ../base_personal.nix

    ../_options/boot-uefi.nix
    ../_options/git.nix
    ../_options/git-gpg.nix
  ];

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = "root";
    allowLocalDeployment = true;
    tags = [];
  };

  # Key stuff just for this laptop.
  networking.hostName = name;

  # firmware upgrades
  services.fwupd.enable = true;

  # for fingerprint scanner
  services.fwupd.package =
    (import (builtins.fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/bb2009ca185d97813e75736c2b8d1d8bb81bde05.tar.gz";
        sha256 = "sha256:003qcrsq5g5lggfrpq31gcvj82lb065xvr7bpfa8ddsw8x4dnysk";
      }) {
        inherit (pkgs) system;
      })
    .fwupd;
}
