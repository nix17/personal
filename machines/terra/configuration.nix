{modulesPath, ...}: let
  name = "terra";
  ip_pub = "192.168.1.7";
in {
  imports = [
    (modulesPath + "/virtualisation/proxmox-lxc.nix")

    ../../applications/server/gitlab.nix
    ../../applications/server/monica.nix
    ../../applications/server/wireguard.nix
    ../../applications/server/vaultwarden.nix
    ../../applications/server/plex.nix
    ../../applications/server/jellyfin.nix
    ../../applications/server/deluge.nix
    ../../applications/server/sonarr.nix
    ../../applications/server/prowlarr.nix
    ../../applications/server/radarr.nix
    ../../applications/server/organizr.nix
  ];

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = "root";
    tags = [];
  };

  networking.hostName = name;

  # will need remote access
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "prohibit-password";
  };

  services.homelab.gitlab.enable = true;
  services.homelab.monica.enable = true;
  services.homelab.wireguard.enable = true;
  services.homelab.vaultwarden.enable = true;
  services.homelab.plex.enable = true;
  services.homelab.jellyfin.enable = true;
  services.homelab.deluge.enable = true;
  services.homelab.sonarr.enable = true;
  services.homelab.prowlarr.enable = true;
  services.homelab.radarr.enable = true;
  services.homelab.organizr.enable = true;
}
