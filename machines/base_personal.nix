{
  pkgs,
  inputs,
  ...
}: {
  imports = [
    inputs.home-manager.nixosModules.default
    ../applications/personal/development.nix
    ../applications/personal/vpn_ul.nix
  ];

  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # enable bluetooth
  hardware.bluetooth.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Dublin";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_IE.UTF-8";
    LC_IDENTIFICATION = "en_IE.UTF-8";
    LC_MEASUREMENT = "en_IE.UTF-8";
    LC_MONETARY = "en_IE.UTF-8";
    LC_NAME = "en_IE.UTF-8";
    LC_NUMERIC = "en_IE.UTF-8";
    LC_PAPER = "en_IE.UTF-8";
    LC_TELEPHONE = "en_IE.UTF-8";
    LC_TIME = "en_IE.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  # Enable the KDE Plasma Desktop Environment.
  services.displayManager.sddm.enable = true;
  services.displayManager.sddm.wayland.enable = true;
  services.desktopManager.plasma6.enable = true;

# GNOME
#  services.xserver.displayManager.gdm.enable = true;
#services.xserver.desktopManager.gnome.enable = true;

# cinnamon
# lightdm
#  services.xserver.displayManager.lightdm.enable = true;
#  #services.displayManager.sddm.wayland.enable = true;
#  services.xserver.desktopManager.cinnamon.enable = true;


  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "ie";
    variant = "";
  };

  # Configure console keymap
  console.keyMap = "ie";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  #sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # for wireguard
  networking.firewall.checkReversePath = false;

  environment.systemPackages = with pkgs; [
    appimage-run
    #python312
    gnupg
    inputs.alejandra.defaultPackage.x86_64-linux
  ];

  # for Obsidian
  nixpkgs.config.permittedInsecurePackages = [
    "electron-25.9.0"
  ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;

    users = {
      silver = {
        home = {
          stateVersion = "23.05";
          packages = with pkgs; [
            firefox
            kate
            thunderbird
            discord
            discord-screenaudio
            filezilla
            teams-for-linux
            libreoffice-qt
            hunspell
            hunspellDicts.en_GB-large
            nextcloud-client
            obsidian
            vlc
            prismlauncher
            whatsapp-for-linux
            teams-for-linux
            plex-media-player
            sieve-editor-gui
            deluge
            element-desktop
            heroic
            kdePackages.kleopatra
            whatsapp-for-linux
            obs-studio
          ];
        };
      };
    };
  };
}
