{inputs, ...}: let
  name = "helios";
  ip_pub = "192.168.1.7";
in {
  imports = [
    inputs.nixos-hardware.nixosModules.lenovo-thinkpad-x1-6th-gen
    ./hardware-configuration.nix
    ../base_personal.nix

    # optionals
    ../_options/boot-grub.nix
    ../_options/git.nix
    ../_options/git-gpg.nix
  ];

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = "root";
    allowLocalDeployment = true;
    tags = [];
  };

  # Key stuff just for this laptop.
  boot.loader.grub.device = "/dev/nvme0n1";
  networking.hostName = name;

  # fingerprint stuff
  # https://github.com/ahbnr/nixos-06cb-009a-fingerprint-sensor
  #  services.fprintd = {
  #    enable = true;
  #    tod = {
  #      enable = true;
  #      driver = inputs.nixos-06cb-009a-fingerprint-sensor.lib.libfprint-2-tod1-vfs0090-bingch {
  #        calib-data-file = ./misc/calib-data.bin;
  #      };
  #    };
  #  };
}
