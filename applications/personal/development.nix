/*
Dev stuff
*/
{...}: {
  programs.adb.enable = true;

  # enable docker
  virtualisation.docker.enable = true;
  users.users.silver.extraGroups = [ "docker" ];

  home-manager.users.silver = {
    pkgs,
    config,
    ...
  }: {
    home = {
      packages = with pkgs; [
        # tools
        jetbrains-toolbox
        jetbrains.clion
        jetbrains.idea-ultimate
        jetbrains.pycharm-professional
        jetbrains.webstorm
        jetbrains.rust-rover
        jetbrains.phpstorm
        jetbrains.goland
        android-studio

        # languages
        # rust
        #pkgs.cargo
        rustc
        rustup
        php

        insomnia
        # apache-directory-studio
        (callPackage ../../packages/apache-directory-studio.nix {})
        jxplorer

        # mockoon
        texliveFull
        texstudio
      ];

      #file.".cargo/bin".source = config.lib.file.mkOutOfStoreSymlink /etc/profiles/per-user/silver/bin;
    };
  };
}
