/*
Config for UL VPN.

Opens a small browser to grab the cookie

Use ``vpn up ul`` and ``vpn down ul``
*/
{inputs, ...}: {
  # https://github.com/NixOS/nixpkgs/issues/231038#issuecomment-1637903456
  environment.etc."ppp/options".text = "ipcp-accept-remote";

  home-manager.users.silver.home = {
    file.".vpn".text = ''
      [
          {
              "name": "ul",
              "host": "ulssl.ul.ie",
              "port": 443,
              "default": true,
              "cert": "22edd86cce8a4d46591f0f8b63f388b98d9abc8a2eb4cd684c85172be066bac2"
          }
      ]
    '';

    packages = [
      inputs.openfortivpn-cli.packages.x86_64-linux.default
    ];
  };
}
