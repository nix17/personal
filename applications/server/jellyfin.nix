{
  config,
  lib,
  ...
}:
with lib; let
  name = "jellyfin";
  cfg = config.services.homelab."${name}";
  domain_full = "${cfg.domain.sub}.${cfg.domain.base}";
in {
  imports = [
    ./acme.nix
    ./nginx.nix
  ];

  options.services.homelab."${name}" = {
    enable = mkEnableOption "Homelab Jellyfin server";

    domain = {
      base = mkOption {
        type = types.str;
        default = "home.brendan.ie";
      };

      sub = mkOption {
        type = types.str;
        default = name;
      };
    };
  };

  config = mkIf cfg.enable {
    # Website config
    services.homelab.acme.domains = [
      domain_full
    ];

    services.nginx.virtualHosts = {
      "${domain_full}" = {
        forceSSL = true;
        useACMEHost = "brendan";
        locations."/" = {
          proxyPass = "http://localhost:8096";
          proxyWebsockets = true;
        };
      };
    };

    services.jellyfin = {
      enable = true;
      openFirewall = true;
    };
  };
}
