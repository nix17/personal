{
  config,
  lib,
  ...
}:
with lib; let
  name = "monica";
  cfg = config.services.homelab."${name}";
  domain_full = "${cfg.domain.sub}.${cfg.domain.base}";
  gitlab_secret = file_path: {
    file = file_path;
    owner = "monica";
    group = "monica";
  };
in {
  imports = [
    ./acme.nix
    ./nginx.nix
  ];

  options.services.homelab."${name}" = {
    enable = mkEnableOption "Homelab Monica";

    domain = {
      base = mkOption {
        type = types.str;
        default = "home.brendan.ie";
      };

      sub = mkOption {
        type = types.str;
        default = name;
      };
    };
  };

  config = mkIf cfg.enable {
    services.homelab.acme.domains = [
      domain_full
    ];

    age.secrets.monica_appKeyFile = gitlab_secret ../../secrets/monica/appKeyFile.age;
    age.secrets.monica_passwordFile = gitlab_secret ../../secrets/monica/passwordFile.age;

    services.monica = {
      enable = true;

      hostname = domain_full;

      appKeyFile = config.age.secrets.monica_appKeyFile.path;

      mail = {
        driver = "smtp";
        encryption = "tls";
        from = "23silver@gmail.com";
        fromName = "Monica";
        host = "smtp.gmail.com";
        passwordFile = config.age.secrets.monica_passwordFile.path;
        port = 587;
        user = "23silver@gmail.com";
      };

      config = {
        MAIL_FROM_ADDRESS= lib.mkForce "monica@brendan.ie";
      };

      maxUploadSize = "1G";

      # no need for anotehr nginx config
      nginx = {
        forceSSL = true;
        useACMEHost = "brendan";
      };
    };
  };
}
