{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  name = "wireguard";
  cfg = config.services.homelab."${name}";
  domain_full = "${cfg.domain.sub}.${cfg.domain.base}";
  port = 51820;
in {
  imports = [
  ];

  options.services.homelab."${name}" = {
    enable = mkEnableOption "Homelab Wireguard";

    domain = {
      base = mkOption {
        type = types.str;
        default = "home.brendan.ie";
      };

      sub = mkOption {
        type = types.str;
        default = name;
      };
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      wireguard-tools
    ];

    services.homelab.acme.domains = [
      domain_full
    ];

    services.nginx = {
      virtualHosts = {
        # not sure how important this is
        "${domain_full}" = {
          forceSSL = true;
          useACMEHost = "brendan";
          locations."/" = {
            proxyPass = "http://localhost:${toString port}";
          };
        };
      };
    };

    age.secrets.wireguard_privateKeyFile.file = ../../secrets/wireguard/privateKeyFile.age;

    networking = {
      # enable NAT
      nat = {
        enable = true;
        externalInterface = "eth0";
        internalInterfaces = ["wg0"];
      };

      # open firewall
      firewall.allowedUDPPorts = [port];

      wireguard.interfaces = {
        # "wg0" is the network interface name. You can name the interface arbitrarily.
        wg0 = {
          # Determines the IP address and subnet of the server's end of the tunnel interface.
          ips = ["10.13.13.1/24"];

          # The port that WireGuard listens to. Must be accessible by the client.
          listenPort = port;

          # This allows the wireguard server to route your traffic to the internet and hence be like a VPN
          # For this to work you have to set the dnsserver IP of your router (or dnsserver of choice) in your clients
          postSetup = ''
            ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.100.0.0/24 -o eth0 -j MASQUERADE
          '';

          # This undoes the above command
          postShutdown = ''
            ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.100.0.0/24 -o eth0 -j MASQUERADE
          '';

          # Path to the private key file.
          privateKeyFile = config.age.secrets.wireguard_privateKeyFile.path;

          peers = [
            {
              # Mobile
              publicKey = "sgJnsKzVzSUGHK9j8sCuoI+aAFsjX4wvS0TVUyTlO24=";
              allowedIPs = ["10.13.13.2/32"];
            }
            {
              # Helios
              publicKey = "fOVXxkhUZ14L+h3FTQxN/cWinIuMAu/c1zVPSgr27WU=";
              allowedIPs = ["10.13.13.3/32"];
            }
            {
              # Aether
              publicKey = "g/5GsnB107XaLDUijDVyvY6XK/11+8Xl8LgowMX/Ujo=";
              allowedIPs = ["10.13.13.4/32"];
            }
          ];
        };
      };
    };
  };
}
