# using K900's one https://gitlab.com/K900/nix/-/blob/a69502b8bf39fd99a85342b2f7989fe5896a6ae0/applications/base/nginx.nix
{pkgs, ...}: let
  temp_site_ip = name: port: ip: {
    "${name}" = {
      forceSSL = true;
      useACMEHost = "brendan";
      locations."/" = {
        proxyPass = "http://${ip}:${port}";
      };
    };
  };
in {
  services.nginx = {
    enable = true;
    package = pkgs.nginxMainline;

    recommendedOptimisation = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;

    statusPage = true;

    # give Nginx access to our certs
    group = "acme";
  };

  networking.firewall.allowedTCPPorts = [80 443];

  services.homelab.acme.domains = [
    "proxmox.home.brendan.ie"
    "pihole.home.brendan.ie"
    "freenas.home.brendan.ie"
    "ip.home.brendan.ie"
  ];

  # temp until everythign is ported
  services.nginx = {
    virtualHosts =
      {
        "proxmox.home.brendan.ie" = {
          forceSSL = true;
          useACMEHost = "brendan";
          locations."/" = {
            proxyPass = "https://192.168.1.4:8006";
          };
        };
      }
      // (temp_site_ip "pihole.home.brendan.ie" "80" "192.168.1.8")
      // (temp_site_ip "freenas.home.brendan.ie" "80" "192.168.1.5");
  };
}
