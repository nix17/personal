{
  config,
  lib,
  ...
}:
with lib; let
  name = "plex";
  cfg = config.services.homelab."${name}";
  domain_full = "${cfg.domain.sub}.${cfg.domain.base}";
in {
  imports = [
    ./acme.nix
    ./nginx.nix
  ];

  options.services.homelab."${name}" = {
    enable = mkEnableOption "Homelab Plex server";

    domain = {
      base = mkOption {
        type = types.str;
        default = "home.brendan.ie";
      };

      sub = mkOption {
        type = types.str;
        default = name;
      };
    };
  };

  config = mkIf cfg.enable {
    # Website config
    services.homelab.acme.domains = [
      domain_full
    ];

    services.nginx.virtualHosts = {
      "${domain_full}" = {
        forceSSL = true;
        useACMEHost = "brendan";
        locations."/" = {
          proxyPass = "http://localhost:32400";
          proxyWebsockets = true;
        };
      };
    };

    services.plex = {
      enable = true;
      openFirewall = true;
      group = "root";
      user = "root";
    };

    # restart plex daily
    systemd.timers."plex_restart" = {
      description = "Restart Plex daily timer";
      wantedBy = ["timers.target"];
      timerConfig = {
        OnCalendar = "*-*-* 22:12:00";
        Unit = "plex_restart.service";
        Persistent = true;
      };
    };

    systemd.services."plex_restart" = {
      description = "Restart Plex daily service";
      wantedBy = [];
      serviceConfig = {
        Type = "oneshot";
        User = "root";
        Group = "root";
        ExecStart = "systemctl restart plex.service";
      };
    };
  };
}
