{
  config,
  lib,
  ...
}:
with lib; let
  name = "organizr";
  cfg = config.services.homelab."${name}";
  domain_full = cfg.domain.base;
in {
  imports = [
    ./acme.nix
    ./nginx.nix
    ./podman.nix
  ];

  options.services.homelab."${name}" = {
    enable = mkEnableOption "Homelab Organizr server";

    domain = {
      base = mkOption {
        type = types.str;
        default = "home.brendan.ie";
      };
    };
  };

  config = mkIf cfg.enable {
    services.homelab.acme.domains = [
      domain_full
    ];

    services.nginx.virtualHosts = {
      "${domain_full}" = {
        forceSSL = true;
        useACMEHost = "brendan";
        locations."/" = {
          proxyPass = "http://localhost:9990";
        };
      };
    };

    virtualisation.oci-containers.backend = "podman";
    virtualisation.oci-containers.containers = {
      organizr = {
        image = "organizr/organizr:latest";
        autoStart = true;
        volumes = [
          "/var/lib/organizr:/config"
        ];
        ports = [
          "9990:80"
        ];
      };
    };
  };
}
