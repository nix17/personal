{
  config,
  lib,
  ...
}:
with lib; let
  name = "gitlab";
  cfg = config.services.homelab."${name}";
  domain_full = "${cfg.domain.sub}.${cfg.domain.base}";
  gitlab_secret = file_path: {
    file = file_path;
    owner = cfg.user;
    group = cfg.user;
  };
in {
  imports = [
    ./acme.nix
    ./nginx.nix
  ];

  options.services.homelab."${name}" = {
    enable = mkEnableOption "homelab Gitlab";

    domain = {
      base = mkOption {
        type = types.str;
        default = "home.brendan.ie";
      };

      sub = mkOption {
        type = types.str;
        default = name;
      };
    };

    user = mkOption {
      type = types.str;
      # changes teh ssh user
      default = "git";
    };
  };

  config = mkIf cfg.enable {
    age.secrets.gitlab_pw = gitlab_secret ../../secrets/gitlab/pw.age;
    age.secrets.gitlab_secrets_db = gitlab_secret ../../secrets/gitlab/secrets_db.age;
    age.secrets.gitlab_secrets_secret = gitlab_secret ../../secrets/gitlab/secrets_secret.age;
    age.secrets.gitlab_secrets_otp = gitlab_secret ../../secrets/gitlab/secrets_otp.age;
    age.secrets.gitlab_secrets_jws = gitlab_secret ../../secrets/gitlab/secrets_jws.age;
    age.secrets.gitlab_db_pw = gitlab_secret ../../secrets/gitlab/db_pw.age;
    age.secrets.gitlab_app_id = gitlab_secret ../../secrets/gitlab/app_id.age;
    age.secrets.gitlab_app_secret = gitlab_secret ../../secrets/gitlab/app_secret.age;

    services.gitlab = {
      enable = true;

      databasePasswordFile = config.age.secrets.gitlab_db_pw.path;
      initialRootPasswordFile = config.age.secrets.gitlab_pw.path;
      secrets = {
        dbFile = config.age.secrets.gitlab_secrets_db.path;
        secretFile = config.age.secrets.gitlab_secrets_secret.path;
        otpFile = config.age.secrets.gitlab_secrets_otp.path;
        jwsFile = config.age.secrets.gitlab_secrets_jws.path;
      };

      user = cfg.user;
      group = cfg.user;
      databaseUsername = cfg.user;

      https = true;
      host = domain_full;
      port = 443;

      extraConfig = {
        omniauth = {
          enabled = true;
          #auto_sign_in_with_provider = "gitlab";
          allow_single_sign_on = ["gitlab"];
          providers = [
            {
              name = "gitlab";
              # label: 'Provider name', # optional label for login button, defaults to "GitLab.com"
              app_id = {_secret = config.age.secrets.gitlab_app_id.path;};
              app_secret = {_secret = config.age.secrets.gitlab_app_secret.path;};
            }
          ];
        };
      };
    };

    services.homelab.acme.domains = [
      domain_full
    ];

    services.nginx = {
      virtualHosts = {
        "${domain_full}" = {
          forceSSL = true;
          useACMEHost = "brendan";
          locations."/" = {
            proxyPass = "http://unix:/run/gitlab/gitlab-workhorse.socket";
            extraConfig = ''
              client_max_body_size 1000M;
            '';
          };
        };
      };
    };

    services.openssh.enable = true;
    systemd.services.gitlab-backup.environment.BACKUP = "dump";
  };
}
