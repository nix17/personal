{
  config,
  lib,
  ...
}:
with lib; let
  name = "acme";
  cfg = config.services.homelab."${name}";
in {
  imports = [];

  options.services.homelab."${name}" = {
    domains = lib.mkOption {
      default = [];
      type = lib.types.listOf lib.types.str;
      description = ''
        A list of domains to use for this server.
      '';
    };
  };

  config = {
    # group that will own the certificates
    users.groups.acme = {};

    age.secrets.acme.file = ../../secrets/dns/cloudflare.age;

    security.acme = {
      preliminarySelfsigned = false;
      acceptTerms = true;

      defaults = {
        email = "letsencrypt_homelab@brendan.ie";
        # we use our own dns authorative server for verifying we own the domain.
        dnsProvider = "cloudflare";
        credentialsFile = config.age.secrets.acme.path;
      };

      certs = {
        "brendan" = {
          domain = "home.brendan.ie";
          extraDomainNames = lists.naturalSort cfg.domains;
        };
      };
    };
  };
}
