{
  config,
  lib,
  ...
}:
with lib; let
  name = "vaultwarden";
  cfg = config.services.homelab."${name}";
  domain_full = "${cfg.domain.sub}.${cfg.domain.base}";
in {
  imports = [
    ./acme.nix
    ./nginx.nix
  ];

  options.services.homelab."${name}" = {
    enable = mkEnableOption "Homelab VaultWarden server";

    domain = {
      base = mkOption {
        type = types.str;
        default = "home.brendan.ie";
      };

      sub = mkOption {
        type = types.str;
        default = "pw";
      };
    };
  };

  config = mkIf cfg.enable {
    # Website config
    services.homelab.acme.domains = [
      domain_full
    ];

    services.nginx.virtualHosts = {
      "${domain_full}" = {
        forceSSL = true;
        useACMEHost = "brendan";
        locations."/" = {
          proxyPass = "http://${toString config.services.vaultwarden.config.ROCKET_ADDRESS}:${toString config.services.vaultwarden.config.ROCKET_PORT}";
          proxyWebsockets = true;
        };
      };
    };

    age.secrets.vaultwarden_details.file = ../../secrets/vaultwarden/environmentFile.age;

    services.vaultwarden = {
      enable = true;
      environmentFile = config.age.secrets.vaultwarden_details.path;
      config = {
        DOMAIN = "https://${domain_full}";
        WEBSOCKET_ENABLED = true;
        SENDS_ALLOWED = true;
        SIGNUPS_ALLOWED = false;
        INVITATION_ORG_NAME = "Homelab";

        ROCKET_ADDRESS = "127.0.0.1";
        ROCKET_PORT = 8222;
        ROCKET_LOG = "critical";
      };
    };
  };
}
